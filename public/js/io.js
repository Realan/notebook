 $(function () {
    var socket = io();

    $('#fsearch').submit(function(){
        socket.emit('search expression', $('#search').val());
        $('#search').val('');
        return false;
    });

    $('#finsert').submit(function(){
        var data = {};
        data.fname =  $('#fname').val();
        data.sname =  $('#sname').val();
        data.lname =   $('#lname').val();
        data.phone =   $('#phone').val();
        
        socket.emit('insert data', data);
        
        $('#fname').val('');
        $('#sname').val('');
        $('#lname').val('');
        $('#phone').val('');
        
        return false;
    });

      socket.on('search result', function(result){
          result.forEach(function(element) {
              $('#table').append($('<tr>').text(element.fname  + " "
                + element.sname + " "
                + element.lname + " "
                + element.phone
                ));
          }, this);
      });


});