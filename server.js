
const express = require("express");
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const MongoClient    = require('mongodb').MongoClient;

const port = 3000;
const url = 'mongodb://localhost:27017/persons';

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('search expression', function(expr){
        console.log('message: ' + expr);

// Search in db  
        MongoClient.connect(url, (err, db) => {
            if (err) {
                console.log('Can\'t connect to MongoDB server. Error:', err);
            } else {
                console.log('Connection!!!');
                var collection = db.collection('data');
                collection.find({$text:{$search:expr}}).toArray(function(err, result) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(result.length);
                            console.log(result);
                            io.emit('search result', result);
                        }; 
                });
                
            };

            db.close;
            console.log('DB closed');
        });
    });

    socket.on('insert data', function(expr){
        console.log('message: ' + expr);

// Insert in db
        MongoClient.connect(url, (err, db) => {
            if (err) {
                console.log('Can\'t connect to MongoDB server. Error:', err);
            } else {
                console.log('Connection!!!');
                var collection = db.collection('data');
                console.log(expr);
                collection.insertOne(expr, function(err, result) {
                        if (err) {
                            console.log(err);
                        } else {

                        }; 
                        
                });
                
            };

            db.close;
            console.log('DB closed');
        });
  

   });

});

http.listen(port, function(){
    console.log('listening on *: ' + port);
});
